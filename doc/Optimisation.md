# MAD Micro-Optimisations
a * (1.0 - b)     == a * -b + a
a * (b - 1.0)     == a * b - a
(a - b) * c       == a * c + (b * -c)
a * (b + 1.0)     == a * b + a
(a + b) * (a - b) == a * a + (-b * b)
(a + b) / c       == a * (1.0 / c) + (b / c)
