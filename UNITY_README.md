# Unity Shader Base
A barebones shader pack designed with flexibility, modularity, and ease of use in mind. Built as a ready-to-go template for other shaders, offering a fleshed out toolset for writing virtually any kind of shader.

# shaderLABs
shaderLABs is a community and network comprised of every active shader pack developer, and is home to every shader pack, within the greater Minecraft community. Whether you're an upcoming developer with thousands of questions, a content creator who wishes to support the community, or a regular user who just wants to hang out, shaderLABs welcomes you with open arms.

https://discord.gg/RpzWN9S
