# Serenity Shader
A shader pack that strives for a natural and serene appearance, while built with configurability in mind.

# Installation
0. Make sure you're on the repository homepage for the shader pack, it should be https://gitlab.com/optifine-shaders/serenity-shader. Make sure you have Optifine installed and working correctly.
1. On the homepage, just under the green line, look for a button just to the right of the "*Web IDE*" button, that looks like a cloud with an arrow sticking out of it, pointing down.
2. Click on this button, and you should be presented with some text saying "Download source code" and the choice of downloading in *.zip*, *.tar.gz*, *.tar.gz2* or *.tar* format.
3. Click *.zip*, and a *.zip* file containing the shader pack should start downloading.
4. Once it has finished downloading, open the folder containing the *.zip* file in your file explorer. Instructions for each web browser are below if you need help. If you don't need help, skip to step 8.
5. If you use Google Chrome, immediately upon downloading the *.zip* file, a little panel should have opened up on the bottom of the web browser, showing the download. Find the *.zip* file, then right click and, and finally click "*Show in folder*". Skip to step 8.
6. If you use Mozilla Firefox, hold "*Control*" on your keyboard, and press "*J*", to open up the downloads window. Find the *.zip* file, it should be just at the top, then right click it, and finally click "*Open Containing Folder*". Skip to step 8.
7. If you use Microsoft Edge, immediately upon downloading the *.zip* file, a panel should have opened at the bottom of the web browser, asking if you would like to "*Open*" or "*Save*" the file. Click "*Save*", wait for it to finish downloading, and click the "*Open folder*" button once it has finished downloading. Skip to step 8.
8. Keeping the downloads folder open, start Minecraft and load into a world.
9. Pause the game, then click "*Options*", then "*Video Settings*", then "*Shaders*", and click the "*Shaders Folder*" button to open the shader packs folder.
10. Open the downloads folder, right click the *.zip* file, then click "*Cut*". Open the shader packs folder, right click on any empty space, the click "*Paste*". The *.zip* file should now be in the shader packs folder.
11. Open Minecraft back up, and the shader pack should now be in the shaders menu.
12. Select the shader pack, wait for it to load, and enjoy!

# shaderLABs
shaderLABs is a community and network comprised of every active shader pack developer, and is home to every shader pack within the greater Minecraft community. Whether you're an upcoming developer with thousands of questions, a content creator who wishes to support the community, or a regular user who just wants to hang out, shaderLABs welcomes you with open arms.

https://discord.gg/RpzWN9S
