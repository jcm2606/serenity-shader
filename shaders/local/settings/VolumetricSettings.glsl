/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_VOLUMETRICS
    #define LOCAL_INCL_SETTINGS_VOLUMETRICS

    /*** Volumetric Lighting Settings ***/

    #define VOLUMETRIC_LIGHTING // Volumetric lighting simulates the scattering of light within a volume of material, such as fog or air, producing visible rays of light.
    #define VL_STEPS 6 // How many steps should be used for volumetric lighting?. More steps can help reduce any visible noise, at the cost of performance. When Temporal AA is enabled, any visible noise is smoothed out, so less steps can help performance for a minimal visual tradeoff. [1 2 3 4 6 8 10 12 14 16]
    //#define VL_COLOURED // Projects coloured shadows cast from transparent surfaces through the volume, tinting the volume the colour of the shadow.
    //#define VL_CLOUD_SHADOW

    #define VL_SKY_LIGHT_NONE 0
    #define VL_SKY_LIGHT_DIRECT 1
    #define VL_SKY_LIGHT_SHADOWING_MODE VL_SKY_LIGHT_DIRECT // [VL_SKY_LIGHT_NONE VL_SKY_LIGHT_DIRECT]

    #define VL_LAYER_ATMOSPHERE

    #define VL_LAYER_AMBIENT_ATMOSPHERE
    #define VL_LAYER_AMBIENT_ATMOSPHERE_DENSITY 150 // [25 50 100 150 200 250 300 350 400 450 500]
    #define VL_LAYER_AMBIENT_ATMOSPHERE_HEIGHT 20 // [5 10 20 30 40 50]

    #define VL_LAYER_HEIGHT_FOG
    #define VL_LAYER_HEIGHT_FOG_DENSITY 600 // [400 600 800 1200 1400 1600 1800 2000 2500 3000 3500 4000]
    #define VL_LAYER_HEIGHT_FOG_HEIGHT 20 // [5 10 20 30 40 50]

    //#define VL_LAYER_GROUND_FOG

#endif
