/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_TAA
    #define LOCAL_INCL_SETTINGS_TAA

    /*** Temporal AA Settings ***/

    #define TAA // Temporal Anti-Aliasing smooths out jagged edges and noise, at the cost of introducing a subtle blur and some ghosting.

    #define TAA_SHARPENING 0.00 // How much sharpening should be applied after Temporal TAA?. Sharpening can help reduce blur and ghosting, but will reintroduce jagged edges and noise. [0.00 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]

#endif
