/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_PARALLAX
    #define LOCAL_INCL_SETTINGS_PARALLAX

    /*** Terrain Parallax Settings ***/

    #define PARALLAX_TERRAIN // Parallax, also called POM, adds some depth to otherwise flat surfaces. Requires a height map provided by a texture pack.
    #define PARALLAX_TERRAIN_DEPTH 0.10 // How deep should parallax be?. [0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]

    #define PARALLAX_TERRAIN_SHADOWS // Casts raytraced shadows through the texture. Doesn't require parallax to be enabled. Requires a height map provided by a texture pack.

#endif
