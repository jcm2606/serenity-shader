/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_WATER
    #define LOCAL_INCL_SETTINGS_WATER

    /*** Volume Settings ***/

    #define VOLUMETRIC_WATER // Volumetric water rendering simulates the scattering of light within the water volume producing visible rays of light. When disabled, the water volume is instead simulated analytically, more akin to older shaders.
    #define VW_STEPS 4 // How many steps should be used for volumetric water rendering?. More steps can help reduce any visible noise, at the cost of performance. When Temporal AA is enabled, any visible noise is smoothed out, so less steps can help performance for a minimal visual tradeoff. [1 2 3 4 6 8 10 12 14 16]
    //#define VW_COLOURED // Projects coloured shadows cast from transparent surfaces through the volume, tinting the volume the colour of the shadow. Also casts projected caustics through the volume, if projected caustics are enabled.
    //#define VW_CLOUD_SHADOW

    #define VW_DENSITY 1.0 // How dense should the water volume be?. Higher densities will result in stronger scattering and absorption of light through the volume. [0.1 0.2 0.3 0.4 0.5 0.7 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0]
    #define VW_SCATTER_STRENGTH 1.0 // [0.7 0.8 0.9 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0 3.5 4.0 4.5 5.0]
    #define VW_ABSORB_STRENGTH 1.0 // [0.7 0.8 0.9 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0 3.5 4.0 4.5 5.0]

    // vec3(0.25422, 0.03751, 0.01150)
    // vec3(0.25422, 0.04751, 0.01150)

    const float waterDensity = 3.0 * VW_DENSITY;

    const vec3 waterScatterCoeff = vec3(0.013) * waterDensity * VW_SCATTER_STRENGTH * rLOG2;
    const vec3 waterAbsorbCoeff  = vec3(0.25422, 0.03751, 0.01150) * waterDensity * VW_ABSORB_STRENGTH * rLOG2;
    const vec3 waterExtinctionCoeff = waterScatterCoeff + waterAbsorbCoeff;

    /*** Caustics Settings ***/

    #define WATER_CAUSTICS_OFF 0
    #define WATER_CAUSTICS_PROJECTED 1
    #define WATER_CAUSTICS_MODE WATER_CAUSTICS_PROJECTED // How should water caustics be simulated?. 'Off' literally disables caustics. 'Projected' simulates the caustics within the shadow map, allowing them to be projected into the world for free. [WATER_CAUSTICS_OFF WATER_CAUSTICS_PROJECTED]

#endif
