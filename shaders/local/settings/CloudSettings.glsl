/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_CLOUD
    #define LOCAL_INCL_SETTINGS_CLOUD

    /*** Volumetric Cloud Settings ***/

    #define VOLUME_CLOUDS
    
    #define VC_ALTITUDE 1800 // [200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000 3500 4000 4500]
    #define VC_HEIGHT 800 // [200 400 600 800 1000 1200 1400 1600 1800 2000 2200 2400]
    #define VC_SCALE 1.0 // [0.2 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0]
    #define VC_SPEED 1.0 // [0.5 1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0]
    #define VC_DENSITY 1.0 // [0.2 0.4 0.6 0.8 1.0 1.2 1.4 1.6 1.8 2.0]
    #define VC_DETAIL 5 // [4 5 6 7]

    #define VC_STEPS 4 // [1 2 3 4 5 6 8 10 12]
    #define VC_MULTISCATTER_STEPS 3 // [2 3 4 5 6]
    #define VC_DIRECT_LIGHT_STEPS 3 // [2 3 4 5 6]
    #define VC_UNUSED_STEPS 3

    #define VC_SKYBOX_STEPS 3 // [1 2 3 4 5 6 8 10 12]
    #define VC_SKYBOX_MULTISCATTER_STEPS 2 // [2 3 4 5 6]
    #define VC_SKYBOX_DIRECT_LIGHT_STEPS 2 // [2 3 4 5 6]
    #define VC_SKYBOX_UNUSED_STEPS 3

    //#define VC_SHADOW
    #define VC_SHADOW_STEPS 2 // [1 2 3 4 5]
    #ifdef VC_SHADOW
    #endif

    const float vc_lowerAltitude = float(VC_ALTITUDE);
    const float vc_height = float(VC_HEIGHT);
    const float vc_rHeight = 1.0 / vc_height;
    const float vc_midAltitude = vc_lowerAltitude + vc_height * 0.5;
    const float vc_upperAltitude = vc_lowerAltitude + vc_height;

    const float vc_scale = 1.4e-4 * (1800.0 / vc_lowerAltitude) / VC_SCALE;
    const float vc_speed = 0.04 * VC_SPEED;
    const float vc_coverageClear = 1.15;
    const float vc_coverageRain = 0.7;
    const float vc_density = 0.25 * VC_DENSITY;

    const float vc_scatterCoeff = 0.02 / log(2.0);
    const float vc_absorbCoeff = 1.;
    const float vc_transmittanceCoeff = vc_absorbCoeff;

#endif
