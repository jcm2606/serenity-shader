/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_VOLUME_CLOUDS
    #define LOCAL_INCL_FRAG_VOLUME_CLOUDS

    #include "/unity/shared/PhysicalSky.glsl"
    #include "/unity/shared/Noise.glsl"

    float CalculateCloudFBM(vec3 worldPosition) {
        float opticalDepth = 0.0;

        //float rounding = sqrt(1.0 - abs(((worldPosition.y - vc_lowerAltitude) * vc_rHeight) * 2.0 - 1.0));
        float rounding = pow(1.0 - (abs(worldPosition.y - vc_midAltitude) * vc_rHeight), 6.0);

        worldPosition *= vc_scale;

        const float rotation = radians(123.75);
        const mat2 rotMatrix = crotate2(rotation);

        const vec2 windDirection = vec2(-1.0, 0.0) * vc_speed;
        vec3 movement = windDirection.xyy * TIME;

        float weight = 2.5;

        for(int i = 1; i <= VC_DETAIL; ++i) {
            worldPosition *= 2.65;
            worldPosition.xy *= rotMatrix;
            worldPosition.yz *= rotMatrix;
            movement *= 1.4;
            weight *= 0.45;

            opticalDepth += noise3D(worldPosition + movement) * weight;
        }

        opticalDepth -= mix(vc_coverageClear, vc_coverageRain, rainStrength);
        
        return saturate(opticalDepth) * saturate(rounding) * vc_density;
    }

    float CalculatePowderScatter(float opticalDepth, float VoL) {
        float powd = 1.0 - exp2(-opticalDepth * 2.0);
        return mix(powd, 1.0, VoL * 0.5 + 0.5);
    }

    float CalculateCloudLightOD(vec3 rayPosition, vec3 increment, float opticalDepth, vec2 dither, const int steps) {
        const_arg float rayLength = (vc_height / steps);
        increment *= rayLength;
        //rayPosition += increment * dither.x;

        opticalDepth = 0.0;
        for(int i = 0; i < steps; ++i, rayPosition += increment) {
            opticalDepth += CalculateCloudFBM(rayPosition);
        }

        return opticalDepth * rayLength * rLOG2;
    }

    float CalculateCloudSkyFixedOD(vec3 rayPosition) {
        float gradient = min(vc_lowerAltitude - rayPosition.y - vc_height, vc_lowerAltitude) * vc_scale * 0.01;

        return gradient * rLOG2 * 0.07;
    }

    vec2 Halton23(int n) {
        float halton2 = float(bitfieldReverse(n) >> 9u) / (exp2(23.0) - 1.0);

        float i = float(n);
        float f = 1.0, r = 0.0;
        while (i > 0.0) {
            f /= 3.0;
            r += f * mod(i, 3);
            i  = floor(i / 3);
        }

        return vec2(halton2, r);
    }

    float CalculateCloudSkyOD(vec3 rayPosition, float opticalDepth, vec2 dither, const int steps, const int samples) {
        #if 1
            vec3 direction = vec3(0.0, 1.0, 0.0);
            direction *= ((vc_midAltitude - rayPosition.y) / direction.y);

            return CalculateCloudFBM(rayPosition + direction);
        #else
            float skyOD = 0.0;
            for(int i = 0; i < samples; ++i) {
                vec2 offset = vec2(dither.x, fract(dither.x * PHI));
                vec2 sequence = fract(Halton23(frameCounter * samples + i) + offset);
                vec3 dir = GenerateCosineVectorSafe(vec3(0,1,0), sequence);

                skyOD += CalculateCloudLightOD(rayPosition, dir, opticalDepth, dither, steps);
            }
            return skyOD * rPI * 0.1 / steps;
        #endif
    }

    float CalculateCloudLightTransmission(float opticalDepth, float density) {
        return exp2(-opticalDepth * density);
    }

    void CalculateCloudScattering(out vec3 cloudScattering, vec3 rayPosition, vec3 dLight, float opticalDepth, float cloudTransmittance, float VoL, vec2 dither, const int msSteps, const int directSteps, const int skySteps) {
        float directOD = CalculateCloudLightOD(rayPosition, shadowLightDirection, opticalDepth, dither, directSteps);
        float skyOD = CalculateCloudSkyOD(rayPosition, opticalDepth, dither, 3, 2);

        float powder = CalculatePowderScatter(opticalDepth, VoL);
        dLight *= powder;

        float scatterCoeff = TransmittedScatteringIntegral(opticalDepth, vc_transmittanceCoeff);

        #define a 0.5
        #define b 0.15
        #define c 0.85

        for(int i = 0; i < msSteps; ++i) {
            float an = pow(a, float(i));
            float bn = pow(b, float(i));
            float cn = pow(c, float(i));

            float phase = Phase2Lobes(VoL * cn);
            scatterCoeff *= an;

            vec3 directLight = dLight * phase * CalculateCloudLightTransmission(directOD, bn);
            vec3 skyLight = skyColour * CalculateCloudLightTransmission(skyOD, bn);

            cloudScattering += (directLight + skyLight) * (scatterCoeff * cloudTransmittance);
        }

        #undef a
        #undef b
        #undef c
    }

    vec3 CalculateVolumeClouds(vec3 image, vec3 direction, vec2 dither, const int steps, const int msSteps, const int directSteps, const int skySteps) {
    #ifndef VOLUME_CLOUDS
        return image;
    #endif
        
        const_arg float rSteps = 1.0 / steps;

        if(
            (direction.y <  0.01 && cameraPosition.y <= vc_lowerAltitude) ||
            (direction.y > -0.01 && cameraPosition.y >= vc_upperAltitude)
        ) return image;

        vec2 upperSphere = rsi(vec3(0.0, atmospherePlanetRadius + eyeAltitude, 0.0), direction, atmospherePlanetRadius + vc_upperAltitude);
        if(upperSphere.y <= 0.0) return image;

        vec2 lowerSphere = rsi(vec3(0.0, atmospherePlanetRadius + eyeAltitude, 0.0), direction, atmospherePlanetRadius + vc_lowerAltitude);

        vec3 startPosition = direction * lowerSphere.y;
        vec3 endPosition = direction * upperSphere.y;

        vec3 rayStep = (endPosition - startPosition) * rSteps;
        vec3 rayPosition = rayStep * dither.x + (startPosition + cameraPosition + gbufferModelViewInverse[3].xyz);

        float stepLength = length(rayStep);

        float VoL = dot(direction, shadowLightDirection);
        vec3 dLight = lightColour;

        vec4 cloud = vec4(vec3(0.0), 1.0);

        for(int i = 0; i < steps; ++i, rayPosition += rayStep) {
            float opticalDepth = CalculateCloudFBM(rayPosition) * stepLength;
            if(opticalDepth <= 0.0) continue; // Skip this step if there's no material to march through.

            CalculateCloudScattering(cloud.rgb, rayPosition, dLight, opticalDepth, cloud.a, VoL, dither, msSteps, directSteps, skySteps);
            cloud.a   *= exp2(-opticalDepth * rLOG2);
        }

        return image * cloud.a + cloud.rgb;
    }

    float CalculateCloudShadowOD(vec3 worldPosition, vec3 direction, vec2 dither, const int steps) {
        const_arg float rSteps = vc_height / steps;
        float stepSize = rSteps / abs(direction.y);

        direction *= stepSize;
        worldPosition += direction * ((vc_lowerAltitude - worldPosition.y) / direction.y);
        //worldPosition += direction * dither.x;

        float opticalDepth = 0.0;
        for(int i = 0; i < steps; ++i, worldPosition += direction) {
            opticalDepth += CalculateCloudFBM(worldPosition);
        }

        return opticalDepth * stepSize * 0.07;
    }

    float CalculateCloudShadows(out float opticalDepth, vec3 worldPosition, vec3 direction, vec2 dither) {
    #if !defined VOLUME_CLOUDS || !defined VC_SHADOW
        opticalDepth = 0.0;
        return 1.0;
    #endif

        const int steps = VC_SHADOW_STEPS;

        float horizonFade = smoothstep(0.125, 0.075, abs(direction.y));

        opticalDepth = CalculateCloudShadowOD(worldPosition, direction, dither, steps);
        return exp2(-opticalDepth * rLOG2) * (1.0 - horizonFade) + horizonFade;
    }

#endif
