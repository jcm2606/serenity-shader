/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_VOLUMETRICS
    #define LOCAL_INCL_FRAG_VOLUMETRICS

    #include "/unity/shared/ShadowConversion.glsl"

    /*** Partial Water Absorption Macro ***/

    #define partialWaterAbsorb ( (isEyeInWater == 1) ? exp2(-waterExtinctionCoeff * distFront) : vec3(1.0) )

    /*** Optical Depth Functions ***/

    vec2 CalculateAtmosphereOD(vec3 worldPosition) {
        return exp2(-max0(worldPosition.y - 63.0) * atmosphereInverseScaleHeights * rLOG2);
    }

    vec2 CalculateAmbientAtmosphereOD(vec3 worldPosition) {
        return exp2(-max0(worldPosition.y - 63.0) / vec2(VL_LAYER_AMBIENT_ATMOSPHERE_HEIGHT) * rLOG2) * VL_LAYER_AMBIENT_ATMOSPHERE_DENSITY;
    }

    float CalculateHeightFogOD(vec3 worldPosition) {
        return exp2(-max0(worldPosition.y - 63.0) / VL_LAYER_HEIGHT_FOG_HEIGHT * rLOG2) * VL_LAYER_HEIGHT_FOG_DENSITY;
    }

    float CalculateGroundFogOD(vec3 worldPosition) {
        return exp2(-abs(worldPosition.y - 63.0) * 1.0 * rLOG2) * 6e4;
    }

    float CalculateNightFogOD(vec3 worldPosition) {
        if(timeNight <= 0.0) return 0.0;
        return exp2(-max0(worldPosition.y - 63.0) / 20.0 * rLOG2) * 6000 * timeNight;
    }

    float CalculateRainFogOD(vec3 worldPosition) {
        if(rainStrength <= 0.0) return 0.0;
        return exp2(-max0(worldPosition.y - 63.0) / 20.0 * rLOG2) * 10000 * rainStrength;
    }

    vec2 CalculateOpticalDepth(vec3 worldPosition) {
        vec2 opticalDepth = vec2(0.0);
    #ifdef VL_LAYER_ATMOSPHERE
        opticalDepth   += CalculateAtmosphereOD(worldPosition);
    #endif
    #ifdef VL_LAYER_AMBIENT_ATMOSPHERE
        opticalDepth   += CalculateAmbientAtmosphereOD(worldPosition);
    #endif
    #ifdef VL_LAYER_HEIGHT_FOG
        opticalDepth.y += CalculateHeightFogOD(worldPosition);
    #endif
    #ifdef VL_LAYER_GROUND_FOG
        opticalDepth.y += CalculateGroundFogOD(worldPosition);
    #endif
        opticalDepth.y += CalculateNightFogOD(worldPosition);
        opticalDepth.y += CalculateRainFogOD(worldPosition);

        return opticalDepth;
    }

    /*** Lighting Functions ***/

    void CalculateVolumeShadow(out vec3 shadowColour, out vec2 visibility, out float depthFront, vec3 shadowPosition, vec3 worldPosition) {
        shadowPosition.xy = DistortShadowPositionProj(shadowPosition.xy);

        depthFront = texture2DLod(shadowtex0, shadowPosition.xy, 0).x;

        visibility.x = float(texture2DLod(shadowtex1, shadowPosition.xy, 0).x > shadowPosition.z);

    #ifdef VL_COLOURED
        visibility.y = float(depthFront > shadowPosition.z);

        bool isWater = texture2D(shadowcolor1, shadowPosition.xy).a * 2.0 - 1.0 > 0.5;

        vec4 sampleColour = texture2D(shadowcolor0, shadowPosition.xy);
        sampleColour.rgb = ToLinear(sampleColour.rgb * 4.0);
        if(!isWater) sampleColour.rgb = sampleColour.rgb * -sampleColour.a + sampleColour.rgb;

        shadowColour = mix(vec3(visibility.x), sampleColour.rgb, saturate(visibility.x - visibility.y));
    #else
        shadowColour = vec3(visibility.x);
    #endif
    }

    vec3 CalculateWaterVolumeAbsorb(vec3 worldPosition, float depthFront) {
        depthFront = depthFront * shadowDepthFactor * 2.0 - shadowDepthFactor;
        depthFront = depthFront * shadowProjectionInverse[2].z + shadowProjectionInverse[3].z;
        depthFront = (transMAD(shadowModelView, worldPosition)).z - depthFront;
        return (depthFront < 0.0) ? exp2(waterExtinctionCoeff * depthFront) : vec3(1.0);
    }

    void CalculateWaterVolumeShadow(out vec3 shadowColour, out vec2 visibility, out float depthFront, vec3 shadowPosition, vec3 worldPosition) {
        shadowPosition.xy = DistortShadowPositionProj(shadowPosition.xy);

        depthFront = texture2DLod(shadowtex0, shadowPosition.xy, 0).x;

        visibility.x = float(texture2DLod(shadowtex1, shadowPosition.xy, 0).x > shadowPosition.z);

    #ifdef VW_COLOURED
        visibility.y = float(depthFront > shadowPosition.z);

        bool isWater = texture2D(shadowcolor1, shadowPosition.xy).a * 2.0 - 1.0 > 0.5;

        vec4 sampleColour = texture2D(shadowcolor0, shadowPosition.xy);
        sampleColour.rgb = ToLinear(sampleColour.rgb * 4.0);
        if(!isWater) sampleColour.rgb = sampleColour.rgb * -sampleColour.a + sampleColour.rgb;

        shadowColour = mix(vec3(visibility.x), sampleColour.rgb, saturate(visibility.x - visibility.y));
    #else
        shadowColour = vec3(visibility.x);
    #endif

        shadowColour *= CalculateWaterVolumeAbsorb(worldPosition, depthFront);
    }

    /*** Marchers ***/

    mat2x3 CalculateVolumetricLight(vec3 viewPosition, vec3 viewPositionFront, vec3 startPosition, vec3 endPosition, vec2 dither, float skyLight) {
    #ifndef VOLUMETRIC_LIGHTING
        return mat2x3(vec3(0.0), vec3(1.0));
    #endif

        const int   steps  = VL_STEPS;
        const float rSteps = 1.0 / steps;

        vec3 worldStep = (endPosition - startPosition) * rSteps;
        vec3 worldPosition = worldStep * dither.x + startPosition;
        float stepLength = flength(worldStep);

        vec3 shadowStart = SceneToShadowPosition(startPosition) * 0.5 + 0.5;
        vec3 shadowEnd = SceneToShadowPosition(endPosition) * 0.5 + 0.5;
        vec3 shadowStep = (shadowEnd - shadowStart) * rSteps;
        vec3 shadowPosition = shadowStep * dither.x + shadowStart;

        float distFront = distance(vec3(0.0), viewPositionFront);

        float VoL = dot(normalize(viewPosition), shadowLightDirectionView);
        vec2 phase = vec2(sky_rayleighPhase(VoL), PhaseG(VoL, 0.7));

        vec3 scatter = vec3(0.0);
        vec3 absorb  = vec3(1.0);

        for(int i = 0; i < steps; ++i, worldPosition += worldStep, shadowPosition += shadowStep) {
            vec3 shadowColour;
            vec2 visibility;
            float depthFront;
            CalculateVolumeShadow(shadowColour, visibility, depthFront, shadowPosition, worldPosition);

            vec2 opticalDepth = CalculateOpticalDepth(worldPosition + cameraPosition) * stepLength;

            mat2x3 scatterCoeffs = mat2x3(
                atmosphereScatteringCoefficients[0] * TransmittedScatteringIntegral(opticalDepth.x, atmosphereAttenuationCoefficients[0]),
                atmosphereScatteringCoefficients[1] * TransmittedScatteringIntegral(opticalDepth.y, atmosphereAttenuationCoefficients[1])
            );

        #ifdef VL_CLOUD_SHADOW
            float cloudShadowOD;
            float cloudShadows = CalculateCloudShadows(cloudShadowOD, worldPosition, shadowLightDirection, dither);
        #else
            const float cloudShadows = 1.0;
        #endif

            vec3 directLight = lightColour * (scatterCoeffs * phase) * shadowColour * cloudShadows;
            vec3 skyLight = skyColour * (scatterCoeffs * vec2(0.25));
        #if VL_SKY_LIGHT_SHADOWING_MODE == VL_SKY_LIGHT_DIRECT
            skyLight *= shadowColour;
        #endif

            scatter += (directLight + skyLight) * partialWaterAbsorb * absorb;
            absorb  *= exp2(-mat2x3(atmosphereAttenuationCoefficients) * (opticalDepth * rLOG2));
        }

        return mat2x3(scatter, absorb);
    }

    mat2x3 CalculateVolumetricWaterAnalytical(vec3 viewPosition, vec3 startPosition, vec3 endPosition, vec2 dither, float skyLight) {
        float dist = distance(startPosition, endPosition);

        vec3 scatter = vec3(0.0);
        vec3 absorb  = exp2(-waterExtinctionCoeff * dist);

        return mat2x3(scatter, absorb);
    }

    float FournierForandPhase(float cosPhi, float n, float mu) {
        float phi = acos(cosPhi);

        // Not sure if this is correct.
        float v = (3.0 - mu) / 2.0;
        float delta = (4.0 / (3.0 * pow2(n - 1))) * pow2(sin(phi / 2.0));
        float delta180 = 4.0 / (3.0 * pow2(n - 1));

        float p1 = 1.0 / (4.0 * PI * pow2(1.0 - delta) * pow(delta, v));
        float p2 = v * (1.0 - delta) - (1.0 - pow(delta, v)) + (delta * (1.0 - pow(delta, v)) - v * (1.0 - delta)) / pow2(sin(phi / 2.0));
        float p3 = ((1.0 - pow(delta180, v)) / (16.0 * PI * (delta180 - 1.0) * pow(delta180, v))) * (3.0 * cosPhi * cosPhi - 1.0);
        return p1 * p2 + p3;
    }

    mat2x3 CalculateVolumetricWater(vec3 viewPosition, vec3 startPosition, vec3 endPosition, vec2 dither, float skyLight) {
    #ifndef VOLUMETRIC_WATER
        return CalculateVolumetricWaterAnalytical(viewPosition, startPosition, endPosition, dither, skyLight);
    #endif

        const int   steps  = VW_STEPS;
        const float rSteps = 1.0 / steps;

        vec3 worldStep = (endPosition - startPosition) * rSteps;
        vec3 worldPosition = worldStep * dither.x + startPosition;
        float stepLength = flength(worldStep);

        vec3 shadowStart = SceneToShadowPosition(startPosition) * 0.5 + 0.5;
        vec3 shadowEnd = SceneToShadowPosition(endPosition) * 0.5 + 0.5;
        vec3 shadowStep = (shadowEnd - shadowStart) * rSteps;
        vec3 shadowPosition = shadowStep * dither.x + shadowStart;

        float VoL = dot(normalize(viewPosition), shadowLightDirectionView);
        float phase = PhaseG(VoL, 0.5);

        vec3 scatter = vec3(0.0);
        vec3 absorb  = vec3(1.0);

        for(int i = 0; i < steps; ++i, worldPosition += worldStep, shadowPosition += shadowStep) {
            vec3 shadowColour;
            vec2 visibility;
            float depthFront;
            CalculateWaterVolumeShadow(shadowColour, visibility, depthFront, shadowPosition, worldPosition);

            vec3 scatterCoeff = waterScatterCoeff * TransmittedScatteringIntegral(stepLength, waterExtinctionCoeff);

        #ifdef VW_CLOUD_SHADOW
            float cloudShadowOD;
            float cloudShadows = CalculateCloudShadows(cloudShadowOD, worldPosition, shadowLightDirection, dither);
        #else
            const float cloudShadows = 1.0;
        #endif

            vec3 directLight = lightColour * phase * shadowColour * cloudShadows;
            vec3 skyLight = skyColour * 0.25 * shadowColour;

            scatter += (directLight + skyLight) * scatterCoeff * absorb;
            absorb  *= exp2(-waterExtinctionCoeff * stepLength);
        }

        return mat2x3(scatter, absorb);
    }

#endif
