/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_TEXTURE_PACK
    #define LOCAL_INCL_FRAG_TEXTURE_PACK

    #define WATER_SMOOTHNESS 1.0 - 0.97

    void ReadSpecularMap(vec4 specularData, vec3 normalTex, ivec2 blockID, float puddleMask, out float roughness, out float reflectance, out float emission, out float textureAO) {
        bool isSpecularMapPresent = dot(specularData.xyz, specularData.xyz) > 0.0;

        roughness = 1.0;
        reflectance = 0.02; // Default reflectance to 0.02 for dielectrics.
        emission = 0.0;
        textureAO = 1.0; // Default texture AO to 1.0.

        #if   TEXTURE_PACK_FORMAT == SPECULAR
            roughness = 1.0 - specularData.x;
            reflectance = 0.02;
            emission = 0.0;
            textureAO = 1.0;
        #elif TEXTURE_PACK_FORMAT == PBR_OLD
            roughness = 1.0 - specularData.x;
            reflectance = mix(0.02, 0.8, specularData.y);
            emission = specularData.z;
            textureAO = 1.0;
        #elif TEXTURE_PACK_FORMAT == PBR_CONTINUUM
            roughness = 1.0 - specularData.z;
            reflectance = specularData.x;
            emission = specularData.w;
            textureAO = 1.0;
        #elif TEXTURE_PACK_FORMAT == PBR_LAB
            roughness = pow(1.0 - specularData.x, 1.0);
            reflectance = specularData.y * specularData.y;
            emission = specularData.w;
            textureAO = pow2(length(normalTex * 2.0 - 1.0));
        #endif

        switch(blockID.x) { // Override material info for certain blocks.
            case 10012: // Water.
                        roughness = WATER_SMOOTHNESS; reflectance = 0.021; break;
            default: break;
        }

        if(puddleMask > 0.0) {
            roughness = mix(roughness, WATER_SMOOTHNESS, puddleMask);
        }
    }

#endif
