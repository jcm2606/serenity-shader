/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    flat out mat3 tbn;

    out mat2 tileInfo;

    out vec3 albedoTint;
    out vec3 tangentDirection;
    out vec3 tangentDirectionView;
    out vec3 worldPosition;

    out vec2 uvCoord;
    out vec2 lmCoord;

    out float defaultAO;

    flat out ivec2 blockID;
    flat out int maskID;

    // IN
    attribute vec4 at_tangent;
    attribute vec3 mc_Entity;
    attribute vec2 mc_midTexCoord;

    // UNIFORM
    uniform mat4 gbufferModelView, gbufferModelViewInverse;

    uniform vec3 cameraPosition;

    uniform vec2 taaJitter;

    uniform float frameTimeCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"
    #include "/local/vertex/VertexDisplacement.vsh"

    // FUNC
    // MAIN

    void main() {
        uvCoord = gl_MultiTexCoord0.xy;
        lmCoord = gl_MultiTexCoord1.xy * 0.00390625;

        blockID = ivec2(mc_Entity.xz);
        maskID = CalculateMaskID(mc_Entity.xz);

        vec3 viewPosition  = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz);
        worldPosition = transMAD(gbufferModelViewInverse, viewPosition) + cameraPosition;
    #if defined gbuffers_terrain
        worldPosition = CalculateVertexDisplacement(worldPosition, blockID);
    #endif

        viewPosition = transMAD(gbufferModelView, worldPosition - cameraPosition);

        vec4 vertexPosition = viewPosition.xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];
    #if defined TAA
        vertexPosition.xy   = taaJitter * vertexPosition.w + vertexPosition.xy;
    #endif

        gl_Position = vertexPosition;

        albedoTint = gl_Color.rgb;
    #if defined gbuffers_hand || defined gbuffers_hand_water
        defaultAO  = 0.5;
    #else
        defaultAO  = pow(gl_Color.a, defaultAOStrength);
    #endif

    #if !defined gbuffers_terrain && !defined gbuffers_water
        vec3 normal  = (gl_NormalMatrix * gl_Normal) * mat3(gbufferModelView);
        vec3 tangent = (gl_NormalMatrix * (at_tangent.xyz / at_tangent.w)) * mat3(gbufferModelView);
    #else
        vec3 normal  = gl_Normal;
        vec3 tangent = at_tangent.xyz / at_tangent.w;
    #endif

        tbn = mat3(tangent, cross(tangent, normal), normal);

        tangentDirection = -normalize((viewPosition * gl_NormalMatrix) * tbn);
        tangentDirectionView = (mat3(gbufferModelViewInverse) * viewPosition) * tbn;
    
    #if defined gbuffers_terrain
        vec2 coordOffset = abs(gl_MultiTexCoord0.xy - mc_midTexCoord.xy);
        tileInfo = mat2(coordOffset * 2.0, mc_midTexCoord - coordOffset);
    #endif
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    /* DRAWBUFFERS:012 */
    layout(location = 0) out vec4 gbuffer0;
    layout(location = 1) out vec4 gbuffer1;
    layout(location = 2) out vec4 gbuffer2;

    // IN
    flat in mat3 tbn;

    in mat2 tileInfo;

    in vec3 albedoTint;
    in vec3 tangentDirection;
    in vec3 tangentDirectionView;
    in vec3 worldPosition;

    in vec2 uvCoord;
    in vec2 lmCoord;

    in float defaultAO;

    flat in ivec2 blockID;
    flat in int maskID;

    // UNIFORM
    uniform sampler2D tex;
    uniform sampler2D normals;
    uniform sampler2D specular;

    uniform sampler2D noisetex;

    uniform mat4 gbufferModelViewInverse;

    uniform vec3 shadowLightPosition;

    uniform float wetness;

    uniform int frameCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"
    #include "/local/fragment/TexturePack.fsh"
    #include "/local/fragment/ParallaxTerrain.fsh"
    #include "/unity/shared/Noise.glsl"

    // FUNC
    vec3 ClampNormal(vec3 n, vec3 v) {
        float NoV = saturate(dot(n, -v));
        return normalize(NoV * v + n);
    }

    float CalculatePuddleMask(vec3 worldPosition) {
        if(wetness <= 0.0) return 0.0;

        worldPosition *= 0.003;

        const int octaves = 5;

        const float rotation = radians(67.5);
        const mat2 rotMatrix = crotate2(rotation);

        float noise = 0.0;

        float strength = 1.0;
        float scale = 1.0;

        for(int i = 0; i < octaves; ++i) {
            worldPosition.xz *= rotMatrix;
            noise += noise2DSmooth(worldPosition.xz * scale).x * strength;

            strength *= 0.4;
            scale *= 2.0;
            
        }

        noise = pow(noise, 8.0);

        return clamp(1.0 - noise, 0.5, 1.0) * wetness;
    }

    // MAIN

    void main() {
    #if defined gbuffers_terrain
        mat2 texD = mat2(dFdx(uvCoord), dFdy(uvCoord));

        vec2 parallaxCoord;
        vec2 texCoord = CalculateParallaxCoords(parallaxCoord, uvCoord, texD, blockID.x);

        vec2 dither = vec2(Bayer16(gl_FragCoord.xy), 256.0);
    #ifdef TAA
        dither.x = fract(dither.x + LinearBayer32(frameCounter));
    #endif

        #define parallaxShadow CalculateParallaxShadow(parallaxCoord, dither, texD, blockID.x)
    #else
        vec2 texCoord = uvCoord;

        #define parallaxShadow 1.0
    #endif

        vec4 albedo = textureParallax(tex, texCoord) * vec4(albedoTint, 1.0);
        if(albedo.a < 0.1) discard;
    #ifdef TEXTURE_PACK_NO_ALBEDO
        albedo.rgb  = vec3(1.0);
    #endif

        float alphaMask = ceil(albedo.a);

        vec4 normalTex = textureParallax(normals, texCoord);
        normalTex.xyz  = dot(normalTex.xyz, normalTex.xyz) <= 0.0 ? vec3(0.5, 0.5, 1.0) : normalTex.xyz;

    #if defined gbuffers_terrain || defined gbuffers_entities
        float puddleMask = CalculatePuddleMask(worldPosition) * pow(lmCoord.y, 2.0);
    #else
        const float puddleMask = 0.0;
    #endif

        vec3 normal = normalTex.xyz * 2.0 - 1.0;
        #ifdef TEXTURE_PACK_NO_NORMALS
             normal = vec3(0.0, 0.0, 1.0);
        #endif
        if(wetness > 0.0) {
             normal = mix(normal, vec3(0.0, 0.0, 1.0), puddleMask);
        }
             normal = tbn * ClampNormal(normal, -tangentDirectionView);

        vec4 specularData = textureParallax(specular, texCoord);

        float roughness, reflectance, emission, textureAO;
        ReadSpecularMap(specularData, normalTex.xyz, blockID, puddleMask, roughness, reflectance, emission, textureAO);

        gbuffer0 = albedo;
        gbuffer1 = vec4(EncodeNormal(normal), Encode2x16F(vec2(roughness, reflectance)), Encode2x16F(vec2(emission, 0.0)), alphaMask);
        gbuffer2 = vec4(Encode2x8(lmCoord), 1.0, Encode2x8(vec2(defaultAO * textureAO, parallaxShadow)), Encode2x8(vec2(float(maskID) * maskMult, alphaMask)));
    }

#endif
