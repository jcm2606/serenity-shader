/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec3 albedoTint;

    out vec2 uvCoord;

    // IN
    // UNIFORM
    uniform mat4 gbufferModelView, gbufferModelViewInverse;
    
    uniform vec3 cameraPosition;

    uniform vec2 taaJitter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        vec3 viewPosition  = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz);
        vec3 worldPosition = transMAD(gbufferModelViewInverse, viewPosition) + cameraPosition;

        viewPosition = transMAD(gbufferModelView, worldPosition - cameraPosition);

        vec4 vertexPosition = viewPosition.xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];
    #if defined TAA
        vertexPosition.xy   = taaJitter * vertexPosition.w + vertexPosition.xy;
    #endif

        gl_Position = vertexPosition;

        albedoTint = gl_Color.rgb;

        uvCoord = gl_MultiTexCoord0.xy;
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    /* DRAWBUFFERS:0 */
    layout(location = 0) out vec4 gbuffer0;

    // IN
    flat in mat3 tbn;

    in vec3 albedoTint;

    in vec2 uvCoord;

    // UNIFORM
    uniform sampler2D tex;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        vec4 albedo = texture2D(tex, uvCoord) * vec4(albedoTint, 1.0);

        gbuffer0 = albedo;
    }

#endif
