/*
    Serenity, by jcm2606
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out mat3x4 skySH;

    out vec3 lightColour;
    out vec3 skyColour;

    out vec2 screenCoord;

    // IN
    // UNIFORM
    uniform sampler2D noisetex;

    uniform mat4 gbufferModelViewInverse;

    uniform vec3 sunDirection;

    uniform float wetness;
    uniform float eyeAltitude;
    uniform float rainStrength;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/shared/PhysicalSky.glsl"
    #include "/unity/vertex/PhysicalSkySH.vsh"

    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;

        lightColour = (GetSunColorZom(sunDirection) + GetMoonColorZom(-sunDirection));

        vec3 viewAbsorb = vec3(1.0);
        skyColour = sky_atmosphere(vec3(0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 10, viewAbsorb);

        CalculateSkySH(viewAbsorb, skyColour);
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    const bool generateShadowMipmap = true;

    // OUT
    /* DRAWBUFFERS:4 */
    layout(location = 0) out vec4 fragColour;

    // IN
    in mat3x4 skySH;

    in vec3 lightColour;
    in vec3 skyColour;

    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex0;
    uniform sampler2D colortex1;
    uniform sampler2D colortex2;
    uniform sampler2D colortex4;
    uniform sampler2D colortex5;
    uniform sampler2D colortex6;

    uniform sampler2D depthtex0;
    uniform sampler2D depthtex1;

    uniform sampler2D shadowtex0;
    uniform sampler2D shadowtex1;
    uniform sampler2D shadowcolor0;
    uniform sampler2D shadowcolor1;

    uniform sampler2D noisetex;

    uniform mat4 gbufferProjection, gbufferProjectionInverse;
    uniform mat4 gbufferModelView, gbufferModelViewInverse;
    uniform mat4 shadowProjection, shadowProjectionInverse;
    uniform mat4 shadowModelView;

    uniform vec3 sunDirection, sunDirectionView;
    uniform vec3 shadowLightDirection, shadowLightDirectionView;
    uniform vec3 cameraPosition;

    uniform vec2 taaJitter;

    uniform float near, far;
    uniform float frameTimeCounter;
    uniform float sunAngle;
    uniform float wetness;
    uniform float eyeAltitude;
    uniform float timeNight;
    uniform float timeSunrise;
    uniform float rainStrength;

    uniform int frameCounter;
    uniform int isEyeInWater;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/InternalSettings.glsl"
    
    #include "/local/shared/Material.glsl"
    #include "/unity/shared/Position.glsl"
    #include "/unity/shared/PhysicalSky.glsl"
    #include "/local/fragment/DiffuseLighting.fsh"
    #include "/local/fragment/SpecularLighting.fsh"
    #include "/local/fragment/Volumetrics.fsh"
    #include "/local/fragment/VolumeClouds.fsh"

    // FUNC
    vec3 CalculateRefractedScreenPosition(Position positionBack, Position positionFront, vec3 normal, float eta) {
    #ifndef REFRACTION
        return positionFront.viewPosition;
    #endif

        vec3 viewVector = normalize(-positionFront.viewPosition);
        vec3 flatNormal = normalize(cross(dFdx(positionFront.viewPosition), dFdy(positionFront.viewPosition)));
        
    #ifdef REFRACTION_PERSPECTIVE_NORMAL
        normal = mat3(gbufferModelView) * -normal;
        float strength = 1.0 / positionFront.viewPosition.z;
    #else
        normal = mat3(gbufferModelView) * normal - flatNormal;
        const float strength = 1.0;
    #endif

        vec3 rayDirection = refract(viewVector, normal, eta);

        vec3 refractedPosition   = rayDirection * abs(distance(positionFront.viewPosition, positionBack.viewPosition)) * strength + positionBack.viewPosition;
             refractedPosition   = ViewToScreenPosition(refractedPosition);
             refractedPosition.z = texture2D(depthtex1, refractedPosition.xy).x;

        return refractedPosition;
    }

    mat2x3 CalculateBackVolume(vec3 viewPosition, vec3 viewPositionFront, vec3 startPosition, vec3 endPosition, vec2 dither, float skyLight, bool isWater) {
        if(isWater || isEyeInWater == 1) {
            return CalculateVolumetricWater(viewPosition, startPosition, endPosition, dither, skyLight);
        } else {
            return CalculateVolumetricLight(viewPosition, viewPositionFront, startPosition, endPosition, dither, skyLight);
        }
    }

    // MAIN

    void main() {
        vec2 coord = screenCoord;
        
        Position positionBack  = GetPosition(coord, depthtex1);
        Position positionFront = GetPosition(coord, depthtex0);

        Material material = GetMaterial(coord);

        vec3 image = DecodeRGBE8(texture2D(colortex4, coord));

        bool isLandBack = isLand(positionBack.depth);
        bool isLandFront = isLand(positionFront.depth);

        bool isTransparent = positionBack.depth > positionFront.depth;

        vec3 refractedPosition = CalculateRefractedScreenPosition(positionBack, positionFront, material.normal, 0.75);

        if(refractedPosition.z > texture2D(depthtex0, refractedPosition.xy).x) {
            coord = refractedPosition.xy;
            positionBack = GetPosition(coord, depthtex1);
            image = DecodeRGBE8(texture2D(colortex4, coord));
            isTransparent = positionBack.depth > positionFront.depth;
            isLandBack = isLand(positionBack.depth);
        }

        vec2 dither = vec2(Bayer16(gl_FragCoord.xy), 256.0);
    #ifdef TAA
        dither.x = fract(dither.x + LinearBayer32(frameCounter));
    #endif

        if(!isLandBack) {
            vec3 sceneDirection = normalize(positionBack.scenePosition);

            vec3 viewAbsorb = vec3(0.0);
            image = sky_atmosphere(vec3(CalculateStars(sceneDirection)), sceneDirection, vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 15, viewAbsorb);

            float VoS = dot(sceneDirection, sunDirection);
            image += CalculateSunSpot(VoS) * skySunColor * viewAbsorb;
            image += CalculateMoonSpot(-VoS) * skyMoonColor * viewAbsorb;
            image  = CalculateVolumeClouds(image, sceneDirection, dither, VC_STEPS, VC_MULTISCATTER_STEPS, VC_DIRECT_LIGHT_STEPS, VC_UNUSED_STEPS);
        }

        mat2x3 volumeFront = CalculateVolumetricLight(positionBack.viewPosition, positionFront.viewPosition, (isEyeInWater == 0) ? gbufferModelViewInverse[3].xyz : positionFront.scenePosition, (isEyeInWater == 0) ? positionFront.scenePosition : positionBack.scenePosition, dither, material.skyLight);

        vec3 shadows = vec3(0.0);
        if(isLandFront) shadows = texture2D(colortex5, screenCoord).rgb;

        mat2x3 volumeBack = mat2x3(vec3(0.0), vec3(1.0));
        if(isTransparent || isEyeInWater == 1) {
            volumeBack = CalculateBackVolume(positionBack.viewPosition, positionFront.viewPosition, (isEyeInWater == 1) ? gbufferModelViewInverse[3].xyz : positionFront.scenePosition, (isEyeInWater == 1) ? positionFront.scenePosition : positionBack.scenePosition, dither, material.skyLight, material.masks.water);
            image = image * volumeBack[1] + volumeBack[0] * float(isEyeInWater == 0);
        }

        if(isTransparent) {
            vec4 transparentGeometry = material.albedo;
            transparentGeometry.rgb  = CalculateDiffuseLighting(material, positionFront.scenePosition, dither, shadows);

            image = mix(image * material.albedo.rgb, transparentGeometry.rgb, transparentGeometry.a);
        }

        if(isLandFront && isEyeInWater == 0) {
            image = CalculateSpecularLighting(positionFront, material, image, shadows, dither);
        }

        image = (image * volumeFront[1] + volumeFront[0]) + (volumeBack[0] * float(isEyeInWater == 1));
        //image = material.normal * 0.5 + 0.5;
        //image = vec3(material.defaultAO);

        fragColour = EncodeRGBE8(image);
    }

#endif
